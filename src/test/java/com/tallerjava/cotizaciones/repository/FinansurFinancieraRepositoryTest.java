package com.tallerjava.cotizaciones.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import java.util.Date;
import junit.framework.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinansurFinancieraRepositoryTest {

    @Autowired
    FinansurFinancieraRepository finansur;
    @Autowired
    CoinDeskFinancieraRepository coindesk;

    @Test
    public void obtenerCotizacion_variasCotizaciones_retornaUltimaCotizacion() {
        Cotizacion cotizacion = null;
        cotizacion = finansur.obtenerCotizacion();
        assertNotNull(cotizacion);
    }

    @Test
    public void obtenerNombre_nombreIgualAFinanSur_retornaFinanSur() {
        String actual = finansur.getNombre();
        Assert.assertEquals("Finan Sur", actual);
    }

    @Test
    public void recuperarCotizacion_variasCotizaciones_retornaUltimaCotizacion() {
        Cotizacion cotizacion = null;
        cotizacion = finansur.recuperarCotizacion(coindesk.getNombre());
        assertNotNull(cotizacion);
    }
    
    @Test
    public void guardarCotizacion_cotizacionOk_seGuardaLaCotizacion() {
        Date fecha = new Date(118, 8, 8);
        double precio = 12.456f;
        String moneda = "USD";
        Cotizacion cotizacion = new Cotizacion(coindesk.getNombre(), fecha, moneda, precio);
        finansur.guardarCotizacion(cotizacion);
    }
}
