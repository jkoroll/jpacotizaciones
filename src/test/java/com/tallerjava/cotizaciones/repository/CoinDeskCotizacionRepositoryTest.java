package com.tallerjava.cotizaciones.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import com.tallerjava.cotizaciones.repository.CotizacionNoObtenidaException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest

public class CoinDeskCotizacionRepositoryTest {

    @Autowired
    CoinDeskFinancieraRepository coinDesk;

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {

        Cotizacion cotizacion = null;
        cotizacion = coinDesk.obtenerCotizacion();
        assertNotNull(cotizacion);
    }

    @Test(expected = CotizacionNoObtenidaException.class)
    public void obtenerCotizacion_urlIncorrecta_httpException() {
        coinDesk.setUrl("https://api.binanc.com/api/v1/ticker/price?symbol=BTCUSDT");
        Cotizacion resultadoObtenido = coinDesk.obtenerCotizacion();
    }
}
