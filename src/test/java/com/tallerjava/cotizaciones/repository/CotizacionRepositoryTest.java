package com.tallerjava.cotizaciones.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CotizacionRepositoryTest {
    @Autowired
    CotizacionRepository repository;

    @Test
    public void findFirts1ByProveedorOrderByFechaDesc_variasCotizaciones_retornaUltimaCotizacion() {
        System.out.print(repository.findFirts1ByProveedorOrderByFechaDesc("Finan Sur").get(0).toString());
    }

}
