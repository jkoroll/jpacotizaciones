package com.tallerjava.cotizaciones.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BinanceCotizacionRepositoryTest {

    @Autowired
    BinanceFinancieraRepository binance;

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {

        Cotizacion cotizacion = null;
        cotizacion = binance.obtenerCotizacion();
        assertNotNull(cotizacion);
    }

    @Test(expected = CotizacionNoObtenidaException.class)
    public void obtenerCotizacion_urlIncorrecta_httpException() {
        binance.setUrl("https://api.coindes.com/v1/bpi/currentprice.json");
        Cotizacion resultadoObtenido = binance.obtenerCotizacion();
    }
}
