package com.tallerjava.cotizaciones.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import com.tallerjava.cotizaciones.repository.CotizacionNoObtenidaException;
import com.tallerjava.cotizaciones.repository.CryptoFinancieraRepository;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CryptoCotizacionRepositoryTest {

    @Autowired
    CryptoFinancieraRepository crypto;

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        Cotizacion cotizacion = null;
        cotizacion = crypto.obtenerCotizacion();
        assertNotNull(cotizacion);
    }

    @Test(expected = CotizacionNoObtenidaException.class)
    public void obtenerCotizacion_urlIncorrecta_httpException() {

        crypto.setUrl("https://min-api.cryptocompar.com/data/price?fsym=BTC&tsyms=USD");
        Cotizacion resultadoObtenido = crypto.obtenerCotizacion();
    }
}
