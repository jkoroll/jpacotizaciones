package com.tallerjava.cotizaciones.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FinansurFinancieraRepository extends FinancieraRepository {

    private String nombre = "Finan Sur";

    @Autowired
    private CotizacionRepository cotizacionRepository;

    @Override
    public Cotizacion obtenerCotizacion() {
        Cotizacion cotizacion;
        try {
            cotizacion = cotizacionRepository.findFirts1ByProveedorOrderByFechaDesc(nombre).get(0);
        } catch (RuntimeException ex) {
            throw new CotizacionNoObtenidaException(ex.getMessage(), ex);
        }
        return cotizacion;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    public Cotizacion recuperarCotizacion(String proveedor) {
        Cotizacion cotizacion;
        try {
            cotizacion = cotizacionRepository.findFirts1ByProveedorOrderByFechaDesc(proveedor).get(0);
        } catch (RuntimeException ex) {
            throw new CotizacionNoRecuperadaException(ex.getMessage(), ex);
        }
        return cotizacion;
    }
    
    public void guardarCotizacion(Cotizacion cotizacion) {
        try {
            cotizacionRepository.save(cotizacion);
        } catch (RuntimeException ex) {
            throw new CotizacionNoGuardadaException(ex.getMessage(), ex);
        }
    }
}
