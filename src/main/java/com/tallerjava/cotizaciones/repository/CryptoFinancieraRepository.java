package com.tallerjava.cotizaciones.repository;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class CryptoFinancieraRepository extends FinancieraRepository {

    private String url = "https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD";
    private String nombre = "Crypto Compare";

    public String getUrl(String url) {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        try {
            Cotizacion cotizacion;
            HttpResponse response = HttpRequest.get(url).send();
            JSONObject namecampo = new JSONObject(response.body());
            double cotizacionBitcoin = namecampo.getDouble("USD");
            String moneda = "USD";
            Date fecha = new Date();
            cotizacion = new Cotizacion(nombre, fecha, moneda, cotizacionBitcoin);
            return cotizacion;
        } catch (RuntimeException ex) {
            throw new CotizacionNoObtenidaException(ex.getMessage(), ex);
        }
    }
}
