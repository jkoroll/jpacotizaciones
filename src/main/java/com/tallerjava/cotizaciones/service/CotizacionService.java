package com.tallerjava.cotizaciones.service;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import com.tallerjava.cotizaciones.repository.CotizacionNoGuardadaException;
import com.tallerjava.cotizaciones.repository.CotizacionNoObtenidaException;
import com.tallerjava.cotizaciones.repository.CotizacionNoRecuperadaException;
import com.tallerjava.cotizaciones.repository.FinancieraRepository;
import com.tallerjava.cotizaciones.repository.FinansurFinancieraRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CotizacionService {

    @Autowired
    private FinansurFinancieraRepository backupCotizacion;

    @Autowired
    private List<FinancieraRepository> repositorios;

    public List<Cotizacion> obtenerCotizaciones() {

        Cotizacion cotizacion;
        List<Cotizacion> cotizaciones = new ArrayList();

        for (FinancieraRepository repositorio : repositorios) {
            try {
                cotizacion = repositorio.obtenerCotizacion();
                cotizaciones.add(cotizacion);
                if (!"Finan Sur".equals(repositorio.getNombre())) {
                    backupCotizacion.guardarCotizacion(cotizacion);
                }
            } catch (CotizacionNoGuardadaException cotizacionNoOGuardadaException) {
                //Loggear futuramente el error
            } catch (CotizacionNoObtenidaException e) {
                try {
                    cotizaciones.add(backupCotizacion.recuperarCotizacion(repositorio.getNombre()));
                } catch (CotizacionNoRecuperadaException cotizacionNoRecuperadaException) {
                    cotizaciones.add(new Cotizacion(repositorio.getNombre(), null, null, 0.0f));
                }
            }
        }
        return cotizaciones;
    }

}
