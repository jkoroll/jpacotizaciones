package com.tallerjava.cotizaciones.controller;

import com.tallerjava.cotizaciones.modelo.Cotizacion;
import com.tallerjava.cotizaciones.service.CotizacionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CotizacionController {

    @Autowired
    private CotizacionService service;

    @GetMapping(value = "/index")
    public String index() {
        return "index";
    }

    @GetMapping(value = "/mostrarcotizacion")
    public String mostrarcotizacion(ModelMap modelo) {
        List<Cotizacion> cotizaciones = service.obtenerCotizaciones();
        modelo.addAttribute("listadoCotizacion", cotizaciones);
        return "mostrarcotizacion";
    }
}
